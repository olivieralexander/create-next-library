# Create Next Library

> CLI to generate a local NextJS library module using RollUp.

![build](https://img.shields.io/bitbucket/pipelines/olivieralexander/create-next-library/master.svg)

## Features

- Easy-to-use CLI
- Handles all modern JS features
- Bundles `cjs` and `es` module formats
- Standard latest versions of `next` and `react`
- [Rollup](https://rollupjs.org/) for bundling
- [Babel](https://babeljs.io/) for transpiling
- Hot-reloading

## Usage

Install the package globally using npm

```bash
$: npm install -g create-next-library
```

Create a new module using a single command

```bash
$: create-next-library
```

Run `npm start` in both `/` and `/dev` folders and watch it magically hot-reload changes in your module and development environment.

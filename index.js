#!/usr/bin/env node

const inquirer = require("inquirer");
const fs = require("fs");
const exec = require("child_process").exec;
const ora = require("ora");
const editJsonFile = require("edit-json-file");
const printMessage = require("print-message");

const QUESTIONS = [
  {
    name: "template",
    type: "list",
    message: "What project template would you like to generate?",
    choices: fs.readdirSync(`${__dirname}/templates`)
  },
  {
    name: "name",
    type: "input",
    message: "Project name:",
    validate: function(input) {
      if (/^([A-Za-z\-\_\d])+$/.test(input)) return true;
      else
        return "Project name may only include letters, numbers, underscores and hashes.";
    }
  }
];

const CurrentDir = process.cwd();

inquirer.prompt(QUESTIONS).then(answers => {
  const name = answers["name"];
  const template = answers["template"];
  const TemplatePath = `${__dirname}/templates/${template}`;

  const files = ora("Generating files");
  try {
    files.start();

    // Try to create the directory folder for the project
    try {
      fs.mkdirSync(`${CurrentDir}/${name}`);
    } catch (e) {
      throw "Unable to create project folder. Maybe it already exists?";
    }

    // Copy files from the template into the new directory
    createDirectoryContents(TemplatePath, name);
    files.succeed();

    const dependencies = ora("Installing dependencies");
    try {
      dependencies.start();

      exec(
        "npm install",
        {
          cwd: `${CurrentDir}/${name}`
        },
        function() {
          dependencies.succeed();

          const project = ora("Setting up project");
          try {
            project.start();
            let BasePackage = editJsonFile(
              `${CurrentDir}/${name}/package.json`
            );

            let DevPackage = editJsonFile(
              `${CurrentDir}/${name}/dev/package.json`
            );

            BasePackage.set("name", name);
            BasePackage.save();

            let dependency = {};
            dependency[name] = "file:..";

            DevPackage.set("dependencies", dependency);
            DevPackage.save();

            project.succeed();

            const DevDependencies = ora(
              "Installing dev environment dependencies"
            );
            try {
              DevDependencies.start();
              exec(
                "npm install",
                {
                  cwd: `${CurrentDir}/${name}/dev`
                },
                function() {
                  DevDependencies.succeed();
                  printMessage([
                    "All done! Run `npm start` inside the " +
                      `/${name}` +
                      " folder",
                    "and `npm start` inside the " +
                      `/${name}/dev/` +
                      " folder from",
                    "a different terminal tab."
                  ]);
                }
              );
            } catch (e) {
              DevDependencies.fail(`Failed installing dependencies: ${e}`);
              return;
            }
          } catch (e) {
            project.fail(`Failed setting up project: ${e}`);
            return;
          }
        }
      );
    } catch (e) {
      dependencies.fail(`Failed installing dependencies: ${e}`);
      return;
    }
  } catch (e) {
    files.fail(e);
    return;
  }
});

function createDirectoryContents(TemplatePath, ProjectPath) {
  const FilesToCreate = fs.readdirSync(TemplatePath);

  FilesToCreate.forEach(file => {
    const OriginalFilePath = `${TemplatePath}/${file}`;

    // Get information about the current file so we can
    // determine if it is a file or folder.
    const stats = fs.statSync(OriginalFilePath);

    if (stats.isFile()) {
      const contents = fs.readFileSync(OriginalFilePath, "utf8");

      const WritePath = `${CurrentDir}/${ProjectPath}/${file}`;

      try {
        fs.writeFileSync(WritePath, contents, "utf8");
      } catch (e) {
        throw "Failed writing one or more files. \
          Make sure you have all necessary permissions for the folder \
          you are trying to create.";
      }
    } else if (stats.isDirectory()) {
      fs.mkdirSync(`${CurrentDir}/${ProjectPath}/${file}`);

      // recursive call
      createDirectoryContents(
        `${TemplatePath}/${file}`,
        `${ProjectPath}/${file}`
      );
    }
  });
}
